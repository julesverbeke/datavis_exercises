import { writable, readable } from "svelte/store";

//design1
export const data1 = writable([]);
export const cities = readable(['Baltimore', 'Chicago', 'Los Angeles']);
export const scales = readable( ['Universal','Specific']);
export const selectedCity = writable('Baltimore');
export const selectedScale = writable('Universal');

//design2
export const data2 = writable([]);

//design3
export const data3 = writable([]);